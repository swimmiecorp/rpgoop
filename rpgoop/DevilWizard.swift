//
//  DevilWizard.swift
//  rpgoop
//
//  Created by Wim van Deursen on 13-12-15.
//  Copyright © 2015 Wim van Deursen. All rights reserved.
//

import Foundation

class DevilWizard: Enemy {
    override var loot: [String] {
        return ["Magic Wand", "Dark Amulet", "Salted Pork"]
    }
    
    override var type: String {
        return "Devil Wizard"
    }
}